## **Summary of Technical Skills**

**Programming Languages:** Python, C++, CSS, Javascript, Java, VBA

**Software Engineering:** Agile Development, Docker, Git, Unix, Make, Linux GCC, AWS Deployment, Teensy/Arduino, Pandas, NumPy, Matplotlib, Vega Visualization
