## **Experience**

### **Switched On,** Austin — *Web Design* / *Sales Manager*

#### August 2015 - December 2017

Floor manager responsible for smooth operation of the sales floor and coordination between sales and repair staff.

- Overhauled website to incorporate in-store and website inventories. Designed and built custom Shopify web-front using Liquid Script, CSS, HTML, and Javascript to create streamlined customer orders.
- Troubleshoot and identify technical problems in professional electronic musical equipment. Perform low-level electronic repairs to speed up turn around time on repairs.
- Built community and customer relations by giving in depth workshops and individual tutorials on highly specialized music equipment.

### **Altared Weddings and Events,** Austin — *DJ* / *MC*

#### September 2016 - October 2018

Live DJ for private corporate events and weddings. Personally responsible for event success and on site management of client relationships.

### **Freelance Audio Engineer,** Austin

#### 2004 - 2014

Studio and live sound reinforcement, Stage management, Logistical planning for a variety of events and spaces.
