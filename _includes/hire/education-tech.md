### **University of Texas,** Austin - *Computer Science*
#### August 2020 - May 2023

Computer Science student pursuing Bachelor Degree. Completed Course work: Data
Structures, Computer Organization and Architecture. Current UT GPA: 3.54.

### **Austin Community College,** Austin — *AAS Computer Science*

#### June 2018 - May 2020

Honors Student. Instruction in Python, C++, Data Structures, System Architecture,
Academic Fresh Start Initiative STEM GPA: 3.88, Overall FSI GPA: 3.81
