### **Austin Community College,** Austin — *AAS* *Commercial Music Management*

#### January 2010 - December 2012

Music Business with emphasis on technical ability in live and studio applications, acoustics, business management, technical writing.
