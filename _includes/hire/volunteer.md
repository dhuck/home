
## Community & Volunteer, Austin
{% assign relatedTag = "community" %}
{% if relatedTag %}
  <div class="grid-xlarge">
    <div class="related__resume_container">
      {% for post in site.tags[relatedTag] limit: 9 %}
          <article class="related__resume_post">
            <a class="related__link" href="{{ post.url | absolute_url }}">
              <h3 class="related__title">{{ post.title }}</h3>
              {% if post.image %}
              <div class="related__img">
                <figure class="absolute-bg" style="background-image: url('{{ post.image }}');">
                  <!--                    <img src="{{ post.image }}" alt="{{ post.title }}" />-->
                </figure>
              </div>
              {% endif %}
              <div>
              </div>
            </a>
            <p class="related__text"> {{ post.description }} </p>
          </article>
      {% endfor %}
    </div>
  </div>
{% endif %}
