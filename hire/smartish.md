---
layout: hire
title: Davin Lawrence Resume
subtitle: Prepared for Smartish
category: static
description: Hire Me
image:
tags:
 - smartish
experience: full
---

## **Objective**

Recent graduate and polymath with significant industry experience seeking position with a company with a dynamic work environment. Extensive experience with Office, Shopify and Liquidscript with strong aptitude in applying my knowledge to new languages and platforms.
