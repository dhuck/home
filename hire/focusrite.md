---
layout: hire
title: Davin Lawrence Resume
subtitle: Prepared for Focusrite
description: Hire Me
image:
tags:
 - focusrite
experience: full
---
## **Objective**

Recent graduate with professional developer experience. Passionate about embedded software and service infrastructure. Lifelong Musician and DJ with over a decade of experience in the music industry from booking and performance to retail. Seeking an opportunity with Focusrite to synthesize these two skillsets as a Junior Software Developer.
