---
layout: hire
title: Davin Lawrence Resume
subtitle: Prepared for Kronologic
category: static
description: Hire Me
image:
tags:
 - kronologic
experience: tech
---

## **Objective**

Recent graduate with significant prior experience development, devops, and microservices. Lifelong self-motived learner with proven ability to bootstrap and develop skills independently. Seeking opportunity to bring my unique skillset to a dynamic and constructive environment and create new and interesting tools and services.
