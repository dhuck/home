---
layout: hire
title: Davin Lawrence Resume
subtitle: Prepared for eBay
category: static
description: Hire Me
image:
tags:
 - ebay
experience: tech
---
## **Objective**

Securing a position with eBay to ensure quality and reliability of services, apply my talents in a constructive environment, and further my skills.
