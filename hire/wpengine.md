---
layout: hire
title: Davin Lawrence Resume
subtitle: Prepared for WP Engine
category: static
description: Hire Me
image:
tags:
 - wpengine
---
## **GOAL**

Securing a position with WP Engine to ensure quality and reliability of services, apply my talents in a constructive environment, and further my skills.

## **Experience**

### **SecondBrain**, Austin — *DevOps / Lead Developer*

#### December 2016 - May 2018

Techstars Music Alumni 2018. Technical lead responsible for designing, developing, and maintaining the backend system for deep learning NLP systems.

- Improved and expanded AWS deployment to ensure reliability and availability of services. Created and deployed custom AMIs to speed up deep learning training prior to the launch of AWS Sagemaker service.
- Managed uninterrupted services through rapid development by utilizing Rancher to deploy dockerized microservices. Oversaw the design and dockerization of company assets.
- Deployed and maintained private Gitlab server for company use. Responsible for enforcing git practices and coordination between frontend and backend developers.
- Managed development team using Python, React, and Javascript to deliver rolling updates to consumer apps.

### **Austin Community College**, Austin — *CIS Tutor*

#### March 2019 - May 2020

- Designed, created, and maintained online tutoring portal to continue tutoring services through the novel coronavirus epidemic.
- Leveraged industry experience to assist students with understanding problems with Python, C++, algorithms and data structures, Unix, JavaScript, Swift, Oracle/SQL, MS Office, HTML, and CSS.

### **Switched On,** Austin — *Web Design* / *Sales Manager*

#### August 2015 - Dec 2017

Floor manager responsible for smooth operation of the sales floor and coordination between sales and repair staff.

- Overhauled website to incorporate in-store and website inventories. Designed and built custom Shopify web-front using Liquid Script, CSS, HTML, and Javascript to create streamlined customer orders.
- Troubleshoot and identify technical problems in professional electronic musical equipment. Perform low-level electronic repairs to speed up turn around time on repairs.
- Built community and customer relations by giving in depth workshops and individual tutorials on highly specialized music equipment.
