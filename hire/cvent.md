---
layout: hire
title: Davin Lawrence Resume
subtitle: Prepared for Smartish
category: static
description: Hire Me
image:
tags:
 - smartish
experience: full
---

## **Objective**

Current undergrad pursuing a Bachelor’s degree with prior industry experience in both tech and live events seeking opportunity to apply the blend of these skill sets to solve real world problems in the event industry at CVent.
