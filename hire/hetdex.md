---
layout: hire
title: Davin Lawrence Resume
subtitle: Prepared for HETDEX
description: Hire Me
image:
tags:
 - hetdex
experience: edu
---
## **Objective**

Current undergraduate Computer Science student with machine learning experience seeking an opportunity to blend my skills with my passion in astronomy by assisting the HETDEX research team.
