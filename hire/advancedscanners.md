---
layout: hire
title: Davin Lawrence Resume
subtitle: Prepared for Advanced Scanners
description: Hire Me
image:
tags:
 - focusrite
experience: tech
---
## **Objective**

Recent graduate and self-educated software engineer with an AI startup background seeking employment with Advanced Scanners to apply my skills in a manner that furthers medical science, serves humanity, and saves lives.
