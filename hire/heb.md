---
layout: hire
title: Davin Lawrence Resume
subtitle: Prepared for HEB
description: Hire Me
image:
tags:
 - heb
 - intern
experience: tech
---
## **Objective**

Seeking Data Engineering Internship with HEB during summer 2021 to apply the skills
I have gained as a UT Computer Science student and as former tech employee. I
believe my prior experience in Python and Data aggregation/analysis combined with
my education will be a major boon to the HEB internship program.
