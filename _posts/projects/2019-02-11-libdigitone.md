---
layout: post
publish: true
title: "libDigitone"
date: 2019-02-11
category: projects
description: An open-source SysEx library to interface MIDI with the Elektron Digitone.
image: /assets/images/libdigitone.jpg
tags:
  - Projects
  - Music
  - MIDI
  - hire
  - wpengine
  - ebay
  - voltatech
  - focusrite
  - kronologic
---
libDigitone is a portable SysEx library written in python for the Elektron
Digitone. This library was started before the Digitone was added to the
Overbridge family of devices. You can find the source code
[here](https://gitlab.com/dhuck/libdigitone).

Future plans include translating the library into C and Swift to allow an easier
integration with desktop computers and Apple devices.
