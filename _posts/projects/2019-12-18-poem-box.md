---
layout: post
publish: true
title: "Poem Box"
date: 2019-08-09
category: projects
description: Python Webscraper displays random poem on eInk screen.
image: /assets/images/poembox-header.jpg
tags:
  - Projects
  - Raspberry Pi
  - Python
  - hire
---
For Christmas 2019, I made my partner a box that displays a random poem on an
e-Ink screen. The main inspiration was a
[short story dispenser](http://www.ala.org/pla/initiatives/shortstorydispensers/information)
that we came across at the Austin Public Library. After seeing this, I saw
several projects on r/arduino of people making their own poem dispensers and the
like. Not wanting to waste paper, I decided to make my own using the Pimoroni
wHAT e-Ink display, a Raspberry Pi Zero W, and an old cigar box.

![E-Ink Screen](/assets/images/poembox-01.jpg)

The result is a box that displays a random poem every 5 minutes in a format
that can be upgraded at a future date. If a poem is too large to fit on the
screen, the button can be used to page forward in the poem, or select a new
poem when the current poem has been completed.

![Button](/assets/images/poembox-02.jpg)

### The Code

The code itself is divided into two parts -- a Jupyter notebook that lives on a
server at my house to collect poems, and a Python library on the Raspberry
Pi which handles the choosing and display of a poem. The whole code library can
be found [on my gitlab](https://gitlab.com/dhuck/poemtry). The jupyter notebook
acts as an
[example of using selenium](https://gitlab.com/dhuck/poemtry/blob/production/doc/Poemtry%20Parser.ipynb)
