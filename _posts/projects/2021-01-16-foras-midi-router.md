---
layout: post
publish: true
title: "Foras"
date: 2021-01-16
category: project
description: The MIDI Interface/routing tool has long been overdue for an update.
image: /assets/images/foras.png
tags:
  - Projects
  - Teensy
  - Programming
  - Lovecraftwork
  - hire
  - intern
---
The MIDI router/interface has long been in dire need of a rework. Most systems
on the market are outdated with bloated software required to operate the device,
or clunky interfaces to set up routing. These devices are designed to be used
with the computer systems as the central aspect, rather than another tool in an
electronic music studio. The DAW-less musician is forced to interact with a
computer to change settings on these devices, removing them from their preferred
workflow. Furthermore, no MIDI interface/router utilizes the full possibility
of modern microprocessors by injecting MIDI effects directly into the device
with a quick control.

The Foras is an offering via [Lovecraftwork](https://davinlawrence.com/lovecraftwork/)
aims to fit this niche as a midi router with 4 MIDI inputs and 4 MIDI
outputs. Each input can be routed to any combination of the outputs with control
change, note on, and clock messages easily filterable. Future revisions will
include modulation sources that can be routed to any NPRN, SysEx, or CC message.
Other tools commonly found in modular synthesizers will also be added, space
allowing, bringing new creative applications into an electronic studio.

At time of this writing, Foras is a prototype with core functionality of MIDI
routing working. Next features to be implemented is preset storage/retrieval,
persistent state across power cycles, and simple LFO creation and routing. More
updates can be found on this page in the future. The code can be reviewed at
[our gitlab](https://gitlab.com/somethinglovecraftian/foras).
