---
layout: post
publish: true
title: "Lovecraftwork"
date: 2018-12-20
category: projects
description: Open source music hardware collective based in Austin, TX
image: /assets/images/somethinglovecraftian.jpg
tags:
  - hetdex
  - Projects
  - Teensy
  - Lovecraftwork
  - voltatech
  - focusrite
---

Lovecraftwork is an open source music hardware and software collective comprised of Chris Holland, Cody Gratner, and myself. We are founded with the philosophy of of creating musical products as software, schematics, kits, and finished products for free or at affordable prices. As electronic musicians first, we are focused on making interesting and novel MIDI/CV controls for musicians. All hardware will be released with the CC-BY-NC-SA license and software will be released using the MIT License as the libraries used allow. You can check the progress of our current projects at [our GitLab repository](https://gitlab.com/somethinglovecraftian).
