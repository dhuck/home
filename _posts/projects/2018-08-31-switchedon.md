---
layout: post
publish: false
title: "Switched On"
date: 2018-08-31
category: projects
description: Custom Shopify theme for Switched On, music retailer in Austin, TX.
image: /assets/images/switchedon.jpg.webp
tags:
  - Web Development
  - Javascript
  - Shopify
  - aldridge
  - wpengine
  - hire
  - smartish
---
Switched On is Austin-based music retailer specializing in the sale of electronic music equipment, instrument repair, and vinyl records. In the summer of 2018, I built a custom theme for Shopify using LiquidScript, SCSS, and Javascript. The theme allows them to dynamically rearrange their front page through a series of cards, allowing for the business to radically change their storefront appearance.

![Switched On View 1](/assets/images/switchedon-grab1.png)

![Switched On View 2](/assets/images/switchedon-grab2.png)

[Visit the store here.](https://switchedonaustin.com)
