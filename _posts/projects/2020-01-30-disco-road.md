---
layout: post
publish: false
title: "Disco Road"
date: 2019-12-20
category: projects
description: Open source music hardware for MIDI/CV control of synthesizers
image: /assets/images/placeholder-8.jpg
tags:
  - Projects
  - Teensy
  - Lovecraftwork
  - voltatech
  - focusrite
---
Disco Road is the first product by [Lovecraftwork](https://davinlawrence.com/lovecraftwork). At it's core, it is simply an empty page which can be written with any type of control the user desires. It is designed from the start to be adaptable in both eurorack and desktop studio set ups. The prototype unit will have USB, 3xDIN MIDI (In/Out/SoftThru), DMX512, and 4x CV with 16-bit DACs to allow for reasonable audio quality.

## Features

- Based on the Teensy 4.0 for rapid development and more than enough power to meet complex tasks.
- Intuitive interface which meets the balance of control and minimal space.
- Modular hardware design will allow Disco Road to be reconfigured to numerous controllers and instruments.
