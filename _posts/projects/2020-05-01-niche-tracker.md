---
layout: post
publish: false
title: "Niche Tracker"
date: 2020-05-01
category: projects
description: Music MIDI Tracker for the Unix Console written in Go.
image: /assets/images/niche.png
tags:

- Golang
- Music
- voltatech
- kronologic
---

Niche Tracker is just that, the most niche possible way to make electronic music. [Music Trackers](https://en.wikipedia.org/wiki/Music_tracker#1987:_origins_on_the_Amiga) were the way to create to music on the Commodore Amiga and have remained popular ever since. As opposed to more common, modern sequencers, trackers display their note and automation data vertically in columns as text. This method allows a lot more information to be transmitted to the user. Niche Tracker follows this tradition, but foregoes a modern GUI and returns to the simplicity of the console. Written completely in Go for the console makes it platform-agnostic and can be easily compiled and run on any system.

Niche Tracker uses VI movements and commands for navigation and input for rapid development of musical sequences. Interactive mode will change how it works depending on the active column. For notes, the keyboard will mimic the note layout popularly found in programs such as Ableton Live and Renoise.

Niche Tracker is in very early development and progress can be found [here](https://gitlab.com/dhuck/niche).
