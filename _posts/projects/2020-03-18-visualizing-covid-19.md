---
layout: post
publish: true
title: "Visualizing Covid 19"
subtitle: Using Pandas and Altair to visualize the spread of COVID-19.
date: 2020-09-01
category: projects
description: Using Pandas and Altair to visualize the spread of COVID-19.
image: /assets/images/covid-header.jpg
tags:
  - Projects
  - Python
  - Data Visualization
  - Pandas
  - hetdex
  - aldridge
  - ebay
  - kronologic
  - intern
---
## Update

Update: The data on this page is no longer being updated. I made this
dashboard as a way to visualize the data early on in the pandemic. I could
not find a source that was visualizing on the county level. As the
pandemic shows no signs of slowing down, there have been a myriad of more
full featured dashboard and visualizations. The numbers are up to date as of
August 20th, 2020.


## Original Post

It goes without saying how serious the COVID-19 pandemic is and how it has
fundamentally changed the way we live. In this time I have had off, I used this
opportunity to deepen my knowledge of both the pandas and altair libraries to
create this dashboard. All data is sourced from
[Jon Hopkins University](https://github.com/CSSEGISandData/COVID-19). As of
March 22nd, the university dropped robust support of tracking recoveries as there
is discrepency in recoveries reporting across different countries. I have
followed suit with this chart. The charts are updated daily at 12:15 AM GMT.

The source for this project can be found
[at my gitlab](https://gitlab.com/dhuck/covid-19). The world chart currently
supports zoom and panning on desktop, and will be supported on mobile in the
near future. State chart with time series data will be available again soon.
<link rel="stylesheet" href="{{ '/assets/css/covid.css' | prepend: site.baseurl }}">

<div id="worldwide-vis"></div>

The recent changes to the Jon Hopkins dataset includes county-level data for the
United States which is a powerful way to track the spread in the United States.
It also highlights how incomplete testing and data reporting is in the United
States.

Because the underlying data groups New York City as one datapoint, the data for
NYC has been split evenly between Kings, Queens, and Richmond County, New York.

<div id="us-vis"></div>

<script src="https://cdn.jsdelivr.net/npm/vega@5.10.0"></script>
<script src="https://cdn.jsdelivr.net/npm/vega-lite@4.7.0"></script>
<script src="https://cdn.jsdelivr.net/npm/vega-embed@6.3.2"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript">

    var world = (function() {
      var json = null;
      $.ajax({
        async: false,
        global: false,
        url: "{{ '/assets/js/covid/world_covid.json' | prepend: site.baseurl }}",
        dataType: "json",
        success: function(data) {
          json = data;
        }
      });
      return json;
    })();

    var us = (function() {
      var json = null;
      $.ajax({
        async: false,
        global: false,
        url: "{{ '/assets/js/covid/counties_covid.json' | prepend: site.baseurl }}",
        dataType: "json",
        success: function(data) {
          json = data;
        }
      });
      return json;
    })();
    var opt = {
        renderer: 'svg',
        actions: false,
        width: 'container',
        padding: 10,
        };

  vegaEmbed('#worldwide-vis', world, opt);
  vegaEmbed('#us-vis', us, opt);


</script>
