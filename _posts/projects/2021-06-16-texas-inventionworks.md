---
layout: post
publish: true
title: "Texas Inventionworks Portal"
date: 2021-06-16
category: project
description: Web App for Texas Inventionworks.
image: /assets/images/texasinventionworks.png
tags:
  - Web
  - hire
  - Programming
---
[Texas Inventionworks](https://texasinventionworks.com) is a maker space embedded in the Cockrell School's Engineering and Education Research Center. Students are provided with a variety of machinery, including 3D printers, manual mills and lathes, laser cutters, and CNC machines. During the covid pandemic, student staff at Texas Inventionworks built a portal to allow students to submit 3D prints remotely and schedule appointments to come in and use the space.

In summer 2021, I was brought on as staff at Texas Inventionworks to work on the portal with [Obinna Akahara](https://obinnaakahara.com/). Over the course of the summer, I redesigned the page to use Material UI rather than homespun components. I implemented tighter security throughout the app and API.

As the project evolves into the future, we will be implementing machine access control. Many of the machines in the space are dangerous and require training before use. The portal will communicate with small, homespun devices on each machine to grant and revoke access to authenticated users.
