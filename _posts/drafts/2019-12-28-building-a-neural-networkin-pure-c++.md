---
layout: post
publish: false
title: "Building a Neural Network in Pure C++"
subtitle: No special libraries to help us out this time.
date: 2019-12-09
category: tutorial
description: Many tutorials on neural networks rely heavily on python and numpy to obfuscate what is happening behind the scenes of a neural network. This tutorial lifts the viel to go deeper.
image: /assets/images/placeholder-9.jpg
tags:
  - Tutorials
  - c++
  - Programming
---
## This is more of a test

Here I want to be testingout the ability for jekyll to do simple syntax 
high-lighting. Also, is there a spell checker for webstorm?

```python
import numpy

def func(a):
    print("Hello World!")
```
