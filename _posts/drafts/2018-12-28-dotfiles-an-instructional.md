---
layout: post
publish: false
title: "Dotfiles"
date: 2018-12-20
category: tutorial
description: Dotfiles are a great way to ensure your shell environment remains constant across several machines. This tutorial goes through my personal dotfile collection.
image: /assets/images/placeholder-1.jpg
tags:
  - Tutorials
  - Linux
  - Programming
---
This is a journey through time and space
